// Testrotator exercises the COVIS rotators and attitude sensor.
package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/go-covis/pkg/sweep"
	"bitbucket.org/uwaploe/go-covis/pkg/translator"
	"github.com/ktr0731/toml"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: testrotator [options] configfile

Move the rotators through a series of steps defined in configfile and
log the results to standard output in CSV format.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	rpcAddr = flag.String("addr", "localhost:10130", "Address of rotator gRPC server")
)

const CSVHDR = "seconds,usecs,pitch,roll,yaw,kPAngle,kRAngle,kHeading"

// Code to implement the TextUnmarshaler interface for `duration`:

type duration struct {
	time.Duration
}

func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

type sysConfig struct {
	Nsamples int           `toml:"nsamples"`
	Interval duration      `toml:"interval"`
	Motion   sweep.Motion  `toml:"motion"`
	Hdg      linearMapping `toml:"heading"`
	Pitch    linearMapping `toml:"pitch"`
	Roll     linearMapping `toml:"roll"`
}

// Angle represents a rotation of the platform
type Angle struct {
	Roll    float64 `json:"roll"`
	Pitch   float64 `json:"pitch"`
	Heading float64 `json:"heading"`
}

// ToPb converts an Angle to a Protocol Buffer message format
func (a *Angle) ToPb() *translator.Orientation {
	return &translator.Orientation{
		Roll:    int32(a.Roll * translator.ANGLESCALE),
		Pitch:   int32(a.Pitch * translator.ANGLESCALE),
		Heading: int32(a.Heading * translator.ANGLESCALE),
	}
}

// State contains the current orientation state of the platform
type State struct {
	T time.Time `json:"t"`
	// Angle of each Rotator in their own coordinate system
	Rotation [3]float64 `json:"rotation"`
	Stalled  uint       `json:"stall_flag"`
	Angle
}

func (s State) String() string {
	return fmt.Sprintf("%d,%d,%.1f,%.1f,%.1f,%.2f,%.2f,%.2f",
		s.T.Unix(),
		s.T.Nanosecond()/1000,
		s.Rotation[translator.TILT],
		s.Rotation[translator.ROLL],
		s.Rotation[translator.PAN],
		s.Pitch,
		s.Roll,
		s.Heading)
}

// Convert a rotation array to a Protocol Buffer Rotation message
func RotationPb(r [3]float64) *translator.Rotation {
	return &translator.Rotation{
		Roll: int32(r[0] * translator.ANGLESCALE),
		Tilt: int32(r[1] * translator.ANGLESCALE),
		Pan:  int32(r[2] * translator.ANGLESCALE),
	}
}

// StateFromPb creates a State struct from a Protocol Buffer message
func StateFromPb(msg *translator.State) State {
	if msg == nil {
		return State{}
	}
	s := State{}
	s.T = time.Now().UTC()
	if msg.R != nil {
		s.Rotation[0] = float64(msg.R.Roll) / translator.ANGLESCALE
		s.Rotation[1] = float64(msg.R.Tilt) / translator.ANGLESCALE
		s.Rotation[2] = float64(msg.R.Pan) / translator.ANGLESCALE
		s.Stalled = uint(msg.R.Stalled)
	}
	if msg.O != nil {
		s.Roll = float64(msg.O.Roll) / translator.ANGLESCALE
		s.Pitch = float64(msg.O.Pitch) / translator.ANGLESCALE
		s.Heading = float64(msg.O.Heading) / translator.ANGLESCALE
	}

	return s
}

type stateFunc func() (*translator.State, error)

// Monitor translator motion and return the final state
func motionMonitor(ctx context.Context, next stateFunc) (*translator.State, error) {
	var (
		state *translator.State
		err   error
	)

	for {
		state, err = next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		select {
		case <-ctx.Done():
			log.Printf("Interrupted")
			return state, ctx.Err()
		default:
		}
	}

	return state, nil
}

func getState(ctx context.Context, client translator.TranslatorClient) (State, error) {
	state, err := client.GetState(ctx, &translator.Empty{})
	if err != nil {
		return State{}, errors.Wrap(err, "GetState")
	}
	return StateFromPb(state), nil
}

func doMove(ctx context.Context, client translator.TranslatorClient,
	angle *Angle) (State, error) {
	o := angle.ToPb()
	stream, err := client.Move(ctx, o)
	if err != nil {
		return State{}, errors.Wrap(err, "start move")
	}
	s, err := motionMonitor(ctx, stream.Recv)
	if err != nil {
		return State{}, errors.Wrap(err, "move")
	}
	return StateFromPb(s), nil
}

func doGoto(ctx context.Context, client translator.TranslatorClient,
	rangles [3]float64) (State, error) {
	r := RotationPb(rangles)
	stream, err := client.Goto(ctx, r)
	if err != nil {
		return State{}, errors.Wrap(err, "start goto")
	}
	s, err := motionMonitor(ctx, stream.Recv)
	if err != nil {
		return State{}, errors.Wrap(err, "goto")
	}
	return StateFromPb(s), nil
}

func doLevel(ctx context.Context, client translator.TranslatorClient) (State, error) {
	stream, err := client.Level(ctx, &translator.Empty{})
	if err != nil {
		return State{}, errors.Wrap(err, "start level")
	}
	s, err := motionMonitor(ctx, stream.Recv)
	if err != nil {
		return State{}, errors.Wrap(err, "level")
	}
	return StateFromPb(s), nil
}

func runPlatform(ctx context.Context, client translator.TranslatorClient,
	m sweep.Motion, interval time.Duration, n int) (<-chan State, error) {
	_, err := doLevel(ctx, client)
	if err != nil {
		return nil, err
	}
	_, err = doLevel(ctx, client)
	if err != nil {
		return nil, err
	}

	state, err := getState(ctx, client)
	if err != nil {
		return nil, err
	}
	dh := m.Start[translator.PAN] - state.Rotation[translator.PAN]
	state, err = doMove(ctx, client, &Angle{Heading: dh})
	if err != nil {
		return nil, err
	}

	var r [3]float64
	r[translator.PAN] = -1
	r[translator.ROLL] = m.Start[translator.ROLL]
	r[translator.TILT] = m.Start[translator.TILT]
	state, err = doGoto(ctx, client, r)
	if err != nil {
		return nil, err
	}

	ch := make(chan State, 1)

	go func() {
		defer close(ch)

		angle := Angle{
			Roll:    m.Increment[translator.ROLL],
			Pitch:   m.Increment[translator.PITCH],
			Heading: m.Increment[translator.HEADING],
		}
		state, err := getState(ctx, client)
		ch <- state
		for i := 0; i < m.Steps; i++ {
			state, err = doMove(ctx, client, &angle)
			if err != nil {
				log.Println(err)
				return
			}
			state, _ = getState(ctx, client)
			log.Printf("Step[%d]; %#v", i+1, state)
			ch <- state
			ticker := time.NewTicker(interval)
			count := n
			for _ = range ticker.C {
				state, _ = getState(ctx, client)
				ch <- state
				count--
				if count <= 0 {
					break
				}
			}
		}

	}()

	return ch, err
}

// Start a gRPC client to control the Translator subsystem.
func grpcClient(address string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		return nil, err
	}

	return conn, err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		fmt.Fprintln(os.Stderr, "Missing configuration file")
		flag.Usage()
		os.Exit(1)
	}

	var cfg sysConfig
	b, err := ioutil.ReadFile(args[0])
	if err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}
	err = toml.Unmarshal(b, &cfg)
	if err != nil {
		log.Fatalf("Error parsing config file: %v", err)
	}

	conn, err := grpcClient(*rpcAddr)
	if err != nil {
		log.Fatal(err)
	}
	client := translator.NewTranslatorClient(conn)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	cfg.Motion.Start[translator.PAN] = cfg.Hdg.Apply(cfg.Motion.Start[translator.PAN])
	cfg.Motion.Start[translator.ROLL] = cfg.Roll.Apply(cfg.Motion.Start[translator.ROLL])
	cfg.Motion.Start[translator.TILT] = cfg.Pitch.Apply(cfg.Motion.Start[translator.TILT])

	ch, err := runPlatform(ctx, client, cfg.Motion, cfg.Interval.Duration, cfg.Nsamples)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintln(os.Stdout, CSVHDR)
	for state := range ch {
		fmt.Fprintf(os.Stdout, "%s\n", state)
	}
}
