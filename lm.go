package main

type linearMapping struct {
	Offset float64 `toml:"offset"`
	Scale  float64 `toml:"scale"`
}

func (lm linearMapping) Apply(x float64) float64 {
	if lm.Scale == 0.0 {
		return x + lm.Offset
	}
	return x*lm.Scale + lm.Offset
}

func (lm linearMapping) Invert(y float64) float64 {
	if lm.Scale == 0.0 {
		return y - lm.Offset
	}
	return (y - lm.Offset) / lm.Scale
}
