module bitbucket.org/uwaploe/testrotator

require (
	bitbucket.org/uwaploe/go-covis v1.16.0
	bitbucket.org/uwaploe/go-reson v1.2.3 // indirect
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/ktr0731/toml v0.3.0
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	google.golang.org/grpc v1.23.0
)
